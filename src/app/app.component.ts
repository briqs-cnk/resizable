import { Component } from '@angular/core';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'resizable';
  state = '';
  size: any = null;
  position: any = null;
  positionValue: Observable<String>;
  onResizing(event) {
    this.positionValue = of(event.size.width + ' X ' + event.size.height);
    this.state = 'Resizing';
    this.size = event.size;
    this.position = event.position;
  }
}
